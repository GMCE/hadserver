################################################################################
#
# domoticz_theme_thinktheme
#
################################################################################

DOMOTICZ_THEME_THINKTHEME_VERSION = 759271cd9c9285d0f8529fdbc2bcba5b5439d423
DOMOTICZ_THEME_THINKTHEME_SITE = git://github.com/DewGew/Domoticz-ThinkTheme.git

DOMOTICZ_THEME_THINKTHEME_DEPENDENCIES = domoticz

define DOMOTICZ_THEME_THINKTHEME_INSTALL_TARGET_CMDS
	rm -rf $(TARGET_DIR)/opt/domoticz/www/styles/had-thinktheme
	cp -r $(@D)/ $(TARGET_DIR)/opt/domoticz/www/styles/had-thinktheme
endef

$(eval $(generic-package))
