all: RPi3

buildroot:
	git submodule init
	git submodule update
	@echo "Remove buildroot/package/domoticz as we prefer our version"
	rm -rf buildroot/package/domoticz
	sed -i '/source "package\/domoticz\/Config.in"/d' buildroot/package/Config.in

RPi%: buildroot
	@echo "Building $@"
	$(MAKE) -f Makefile.$@
	ln -sf $@/output/images/sdcard.img $@_sdcard.img

%_doc: 
	$(MAKE) -f Makefile.$(subst _doc,,$@) graph-size graph-build graph-depends

%_rebuild:
	$(MAKE) -f Makefile.$(subst _rebuild,,$@) meta_hadserver-dirclean busybox-dirclean
	$(MAKE) $(subst _rebuild,,$@)

%_rebuild_full: %_clean
	$(MAKE) $(subst _rebuild_full,,$@)

%_clean:
	$(MAKE) -f Makefile.$(subst _clean,,$@) clean
	rm -f $(subst _clean,,$@)_sdcard.img

%_clean_full:
	rm -rf $(subst _clean_full,,$@)/output

doc: RPi3_doc

rebuild: RPi3_rebuild

rebuild_full: RPi3_rebuild_full

clean: RPi3_clean

clean_full: RPi3_clean_full

.PHONY: RPi% buildroot
