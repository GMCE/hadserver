################################################################################
#
# domoticz_zigate
#
################################################################################

DOMOTICZ_ZIGATE_VERSION = stable5-5.1.016
DOMOTICZ_ZIGATE_SITE = git://github.com/pipiche38/Domoticz-Zigate.git

DOMOTICZ_ZIGATE_DEPENDENCIES = domoticz

define DOMOTICZ_ZIGATE_INSTALL_TARGET_CMDS
	rm -rf $(TARGET_DIR)/opt/domoticz/plugins/zigate/
	cp -r $(@D)/ $(TARGET_DIR)/opt/domoticz/plugins/zigate
	chmod +x $(TARGET_DIR)/opt/domoticz/plugins/zigate/plugin.py
endef

$(eval $(generic-package))
