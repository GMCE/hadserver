################################################################################
#
# domoticz_theme_aurora
#
################################################################################

DOMOTICZ_THEME_AURORA_VERSION = b11bca72aa9e85fe0c43b61058d7f530ec8564bb
DOMOTICZ_THEME_AURORA_SITE = git://github.com/flatsiedatsie/domoticz-aurora-theme.git

DOMOTICZ_THEME_AURORA_DEPENDENCIES = domoticz

define DOMOTICZ_THEME_AURORA_INSTALL_TARGET_CMDS
	rm -rf $(TARGET_DIR)/opt/domoticz/www/styles/had-aurora
	cp -r $(@D)/ $(TARGET_DIR)/opt/domoticz/www/styles/had-aurora
endef

$(eval $(generic-package))
