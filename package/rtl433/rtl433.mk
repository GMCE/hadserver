################################################################################
#
# rtl433
#
################################################################################

RTL433_VERSION = 21.05
RTL433_SITE = git://github.com/merbanan/rtl_433
RTL433_METHOD = git

#RTL433_VERSION = 20.11
#RTL433_SOURCE = $(RTL433_VERSION).tar.gz
#RTL433_SITE = https://github.com/merbanan/rtl_433/archive

#RTL433_INSTALL_STAGING = YES
#RTL433_AUTORECONF = YES
RTL433_CONF_OPTS = 
RTL433_DEPENDENCIES = librtlsdr


$(eval $(cmake-package))
