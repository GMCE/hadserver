################################################################################
#
# zigate_flasher
#
################################################################################

ZIGATE_FLASHER_VERSION = master
ZIGATE_FLASHER_SITE = https://gitlab.com/dpocock/zigate-flasher.git
ZIGATE_FLASHER_DEPENDENCIES = python-serial

define ZIGATE_FLASHER_INSTALL_TARGET_CMDS
	cp -r $(@D)/zigate-flasher.py $(TARGET_DIR)/usr/bin/
	chmod +x $(TARGET_DIR)/usr/bin/zigate-flasher.py
endef

$(eval $(generic-package))
