################################################################################
#
# domoticz_theme_flat
#
################################################################################

DOMOTICZ_THEME_FLAT_VERSION = cb9909547cb179b41c493eac5e5687ae8afd423a
DOMOTICZ_THEME_FLAT_SITE = git://github.com/mixmint/domoticz-flat-theme.git
DOMOTICZ_THEME_FLAT_DEPENDENCIES = domoticz

define DOMOTICZ_THEME_FLAT_INSTALL_TARGET_CMDS
	rm -rf $(TARGET_DIR)/opt/domoticz/www/styles/had-flat
	cp -r $(@D)/ $(TARGET_DIR)/opt/domoticz/www/styles/had-flat
endef

$(eval $(generic-package))
