################################################################################
#
# domoticz_plugin_domoticz_plugins_manager
#
################################################################################

DOMOTICZ_PLUGIN_DOMOTICZ_PLUGINS_MANAGER_VERSION = a599e5c82ad49f0d6eeeee84fc6dd2127876d102
DOMOTICZ_PLUGIN_DOMOTICZ_PLUGINS_MANAGER_SITE = git://github.com/stas-demydiuk/domoticz-plugins-manager.git

DOMOTICZ_PLUGIN_DOMOTICZ_PLUGINS_MANAGER_DEPENDENCIES = domoticz

define DOMOTICZ_PLUGIN_DOMOTICZ_PLUGINS_MANAGER_INSTALL_TARGET_CMDS
	rm -rf $(TARGET_DIR)/opt/domoticz/plugins/had-domoticz_plugins_manager
	cp -r $(@D)/ $(TARGET_DIR)/opt/domoticz/plugins/had-domoticz_plugins_manager
	chmod +x $(TARGET_DIR)/opt/domoticz/plugins/had-domoticz_plugins_manager/plugin.py
endef

$(eval $(generic-package))
