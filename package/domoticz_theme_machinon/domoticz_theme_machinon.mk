################################################################################
#
# domoticz_theme_machinon
#
################################################################################

DOMOTICZ_THEME_MACHINON_VERSION = v1.16.2
DOMOTICZ_THEME_MACHINON_SITE = git://github.com/domoticz/machinon.git

DOMOTICZ_THEME_MACHINON_DEPENDENCIES = domoticz

define DOMOTICZ_THEME_MACHINON_INSTALL_TARGET_CMDS
	rm -rf $(TARGET_DIR)/opt/domoticz/www/styles/had-machinon
	cp -r $(@D)/ $(TARGET_DIR)/opt/domoticz/www/styles/had-machinon
endef

$(eval $(generic-package))
